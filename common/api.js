import store from '../store/index';
export default {
// qq 951449465
	 BASEURI: 'http://www.bl66.cc/',
	
	ESURI: 'http://192.168.1.169:8081/api/',
	h5Appid: 'wxb4660f37187c0b8e',

	es: {
		searchList: 'search',
	},
	/**
	 * 接口名称
	 */
	index: {
		getNotice:'api/index/notice', //通用公告
		inRoom:'api/index/inroom',//进入房间
		roomInfo:'api/index/roominfo',//房间信息
		openAward:'api/Openaward/index',//开奖信息
		userInfo:'api/index/userinfo',//用户信息
		getIssues:'api/index/getissues',//获取最近一期
		getOdds:'api/odds/getOdds',//获取赔率
		bindRoom:'api/index/bindroom',//绑定房间
		betting:'api/index/betting',//下注
		moneyLog:'api/moneylog/index',//资金日志
		crash:'api/moneylog/crash',//充值
		checkin:'api/index/checkin',//验证授权码
		uwithdraw:'api/moneylog/uwithdraw',//上下分申请
		withdraw:'api/moneylog/withdraw',//上下分
	    getbetList:'api/moneylog/getbetlist',//下注记录
	    cancelBet :'api/index/cancelBet',//取消下注
		login:'api/user/login',//登录
		register:'api/user/register',//注册
		
		getKey:'api/index/getKey',//获取键盘
		
		
		alipay:'api/user/alipay',//支付宝
		nickname:'api/user/setnickname',//昵称设置
		getMessages:'api/index/messages',//
		submitLocaltion:'single/home/submitLocaltion',
		getAppletOpenId: 'applet/getAppletOpenId', // 获取openId
		getWxPhone: 'applet/getWxPhone', // 获取手机号

		appletLogin_by_weixin: 'api/user/login_by_weixin', // 登录(手机号:phone 密码:password)
		appLogin : 'single/home/appLogin',
		simpleReg: 'single/home/simpleReg', // 登录(手机号:phone 密码:password)
		home: 'single/home/content', //首页展示
		home1: 'single/home/content1', //首页展示
		homeFlashPromotionList: 'pms/homeFlashPromotionList', // 秒杀列表
		bannerList: 'index/banners', // 首页banner
		updatePassword: 'single/home/updatePassword', // 修改密码
		loginByCode: 'single/home/loginByCode', // 手机和验证码登录
		reg: 'single/home/reg', // 注册
		sendCodes: 'single/home/sms/codes', // 获取验证码
		acceptCoupon: 'single/sms/add', // 获取优惠券
		listMemberCoupon: 'single/sms/listMemberCoupon', // 优惠券列表
		couponList: 'single/home/couponList', //  可领取的优惠券

		groupActivityDetail: 'single/sms/group.activity.getdetial', // 查询团购详情信息
	    groupActivityList: 'single/sms/groupActivityList', // 查询商品团购列表
		logs: 'single/home/logs', //  记录日志

	},
	member: {
		

	},
	goods: {
		
	},
	order: {
		


	},
	cms: {
		
	},

	/**
	 * 封装请求（async await 封装uni.request）
	 * method	   post/get
	 * endpoint    接口方法名
	 * data		   所需传递参数
	 * load		   是否需要loading
	 */
	async apiCall(method, endpoint, data, load) {
		if (load) {
			uni.showLoading({
				title: '请稍候',
				mask: true
			});
		}

		let token = uni.getStorageSync('token') || '';
		let fullurl = this.BASEURI + endpoint;
		var contentType = 'application/x-www-form-urlencoded';
		data.storeid=1;
		data.authorization=token;
		//console.log(endpoint);
		let [error, res] = await uni.request({
			url: fullurl,
			data: data,
			method: method,
			header: {
				'storeid': 1,
				//'Content-Type': 'application/x-www-form-urlencoded',
				 'content-type': contentType,
				// 'authorization1': Authorization || ''
			},
		});
		if (load) {
			uni.hideLoading();
		}
	//console.log(res);
	//console.log(res.statusCode);
	//console.log(error);
		if (undefined==res||'undefined'==res){
			uni.navigateTo({
				url: `/pages/index/index`
			})
		}
		if (res.data.msg == 'User token expired!') {
			uni.showToast({
				title: '请先登录',
				icon: 'none'
			});
			uni.navigateTo({
				url: `/pages/public/login`
			})
		}
		if (res.data.msg == '请登录后操作' || res.data.code == 100) {
			//console.log(res.data);
			uni.showToast({
				title: '请先登录',
				icon: 'none'
			});
			uni.navigateTo({
				url: `/pages/public/login`
			})
		}

		if (res.statusCode == 200) {
			return res.data;
		} else {
			//console.log(res.data);
			if (res.data && res.msg){

				uni.showToast({
					title: res.msg,
					icon: 'none'
				});
				this.$api.msg(res.msg);
			}

		}
	},
	/**
	 * 封装请求（async await 封装uni.request）
	 * method	   post/get
	 * endpoint    接口方法名
	 * data		   所需传递参数
	 * load		   是否需要loading
	 */
	async apiEsCall(method, endpoint, data, load) {
		if (!load) {
			uni.showLoading({
				title: '请稍候',
				mask: true
			});
		}

		let fullurl = this.ESURI + endpoint;
		//let fullurl = 'http://localhost:8085/api/' + endpoint;
		let Authorization = `${store.state.userInfo.tokenHead}${store.state.userInfo.token}`;
		let [error, res] = await uni.request({
			url: fullurl,
			data: data,
			method: method,
			header: {
				'storeid': 1,
				'Content-Type': 'application/x-www-form-urlencoded',
				// 'content-type': 'application/json',
				'Authorization': Authorization || ''
			},
		});
		console.log(error);
		if (!load) {
			uni.hideLoading();
		}
		return res;

	},
}
